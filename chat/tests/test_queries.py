import pytest

from graphql.execution.executors.asyncio import AsyncioExecutor


@pytest.mark.asyncio
async def test_query_conversation(client, requests):
    query = '''
        query {
            conversation (filter: {
                limit: 3
            }) {
                Id
                isActive
                participant
            }
        }
    '''

    executed = await client.execute(
        query,
        context=requests,
        executor=AsyncioExecutor()
    )

    assert executed['data']['conversation']
