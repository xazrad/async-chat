import asyncio

from aiohttp_graphql import GraphQLView
from graphql.execution.executors.asyncio import AsyncioExecutor
from graphql_ws.aiohttp import AiohttpSubscriptionServer

from aiohttp import web

from .graph import schema


subscription_server = AiohttpSubscriptionServer(schema)


async def subscriptions(request):
    token = request.match_info['token']

    token_generator = request.app['ws_token']
    user_id = token_generator.check(request, token)

    # валидацию не прошли
    if user_id is None:
        raise web.HTTPForbidden(
            reason='Invalid authorization token',
        )

    ws = web.WebSocketResponse(protocols=('graphql-ws',))
    await ws.prepare(request)

    await subscription_server.handle(ws)
    return ws


def setup_routes(app):
    # setup graphql
    GraphQLView.attach(
        app,
        schema=schema,
        executor=AsyncioExecutor(loop=asyncio.get_event_loop()),
        route_path='/v1/graphql',
        enable_async=True,
        graphiql=True,
        # middleware=  # TODO: переделать 
    )

    app.router.add_get('/v1/subscriptions/{token}', subscriptions)
