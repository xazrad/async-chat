import pytest

from graphql.execution.executors.asyncio import AsyncioExecutor


@pytest.mark.asyncio
@pytest.mark.parametrize('conversation_id, status', [
    ('20', False),
    ('31', True),
])
async def test_typing_message(client, requests, conversation_id, status):
    query = '''
        mutation ($conversationId: String!) {
            typingMessage(conversationId: $conversationId) {
              ok
              error
            }
        }
    '''

    executed = await client.execute(
        query,
        variables={'conversationId': conversation_id},
        context=requests,
        executor=AsyncioExecutor()
    )

    assert executed['data']['typingMessage']['ok'] == status


async def test_ws_token(client, requests):
    query = '''
        mutation {
            wsToken {
              ok
              token
            }
        }
    '''

    executed = await client.execute(
        query,
        context=requests,
        executor=AsyncioExecutor()
    )

    assert executed['data']['wsToken']['token']


@pytest.mark.asyncio
@pytest.mark.parametrize('conversation_id, status', [
    ('20', False),
    ('31', True),
])
async def test_send_message(client, requests, conversation_id, status):
    query = '''
        mutation ($conversationId: String!, $content: String!){
            sendMessage(conversationId: $conversationId, content: $content) {
              ok
              error
              message {
                  Id
                  direction
                  type
                  timeCreated
                  content
                  isReaded
              }
            }
        }
    '''

    executed = await client.execute(
        query,
        context=requests,
        variables={'conversationId': conversation_id, 'content': 'Привет'},
        executor=AsyncioExecutor()
    )

    assert executed['data']['sendMessage']['ok'] == status
    if status:
        assert executed['data']['sendMessage']['message']


@pytest.mark.asyncio
@pytest.mark.parametrize('conversation_id, status', [
    ('20', False),
    ('31', True),
])
async def test_message_readed(client, requests, conversation_id, status):
    query = '''
        mutation ($conversationId: String!, $messageIds: [String]!){
            messageReaded(conversationId: $conversationId, messageIds: $messageIds) {
              ok
              error
              matchedCount
            }
        }
    '''

    executed = await client.execute(
        query,
        context=requests,
        variables={'conversationId': conversation_id, 'messageIds': ['120', '121', '122', '123', '124']},
        executor=AsyncioExecutor()
    )

    assert executed['data']['messageReaded']['ok'] == status
    if status:
        assert executed['data']['messageReaded']['matchedCount']
