import graphene

from chat.db import Message
from chat.graph.decorators import check_access_conversation
from chat.graph.types import MessageDirectionEmun, MessageReadedOutput, ResultStatusOutput, SendMessageOutput, WsTokenOutput
from chat.utils import dumps


class TypingMessage(graphene.Mutation):
    class Arguments:
        conversation_id = graphene.String(required=True, description="Идентификатор диалога")

    Output = ResultStatusOutput

    @classmethod
    @check_access_conversation
    async def mutate(cls, root, info, converstion_id, **kwargs):
        return ResultStatusOutput(ok=True)


class WSToken(graphene.Mutation):
    Output = WsTokenOutput

    @classmethod
    async def mutate(cls, root, info):
        request = info.context['request']

        token_generator = request.app['ws_token']
        token = token_generator.generate(request)

        return WsTokenOutput(ok=True, token=token)


class MessageReaded(graphene.Mutation):
    class Arguments:
        conversation_id = graphene.String(required=True, description="Идентификатор диалога")
        message_ids = graphene.List(
            graphene.NonNull(graphene.String),
            required=True,
            description="Список идентификаторов сообщений"
        )

    Output = MessageReadedOutput

    @classmethod
    @check_access_conversation
    async def mutate(cls, root, info, conversation_id, message_ids, conv_doc):
        # TODO: реализовать Subscription
        # TODO: отсылать только для пользовательских сообщений
        request = info.context['request']

        mess_coll = Message(request.app['db'])
        # отчитаться о прочтении может только получатель и не учитывает системные сообщения
        matched_count = await mess_coll.make_readed(conversation_id, recipient=request['user_id'], ids=message_ids)
        return MessageReadedOutput(ok=True, matched_count=matched_count)


class SendMessage(graphene.Mutation):
    class Arguments:
        conversation_id = graphene.String(required=True, description="Идентификатор диалога")
        content = graphene.String(required=True, description="Текст сообщения")

    Output = SendMessageOutput

    @classmethod
    @check_access_conversation
    async def mutate(cls, root, info, conversation_id, content, conv_doc):

        request = info.context['request']
        # найдем получателя
        participants = conv_doc['participants'][:2]
        participants.remove(request['user_id'])
        recipient = participants[0]

        mess_coll = Message(request.app['db'])
        mess_output = await mess_coll.create_message(
            conversation_id=conversation_id,
            sender=request['user_id'],
            recipient=recipient,
            content=content
        )
        mess_output['direction'] = MessageDirectionEmun.output.value
        # TODO: доделать
        # Уведомим subscription
        # publish_data = dumps({
        #     "conversation_id": conversation_id,
        #     "sender": sender,
        #     **output
        # })

        # await app['redis_pub'].publish(f'recipient:{recipient}', publish_data)

        return SendMessageOutput(ok=True, message=mess_output)
