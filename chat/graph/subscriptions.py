import asyncio
import random

import graphene
from graphql import ResolveInfo

from .types import RandomType


class ChatSubscription(graphene.ObjectType):
    random_int = graphene.Field(RandomType, description="Для тестирования подписки")

    async def resolve_random_int(self, info: ResolveInfo) -> RandomType:
        i = 0
        while True:
            yield RandomType(seconds=i, random_int=random.randint(0, 500))
            await asyncio.sleep(1.)
            i += 1
