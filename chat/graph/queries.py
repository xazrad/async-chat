import graphene

from chat.db import Conversation

from .types import ConversationInput, ConversationObject


class ConversationQuery(graphene.ObjectType):
    conversation = graphene.List(
        ConversationObject,
        filter=ConversationInput(required=True)
    )

    @classmethod
    async def resolve_conversation(cls, root, info, filter):
        request = info.context['request']

        query = {}
        if filter._id:
            query['_id'] = filter._id
        elif filter.participant:
            query["participants"] = {'$all': [filter.participant, request['user_id']]}
        else:
            query["participants"] = request['user_id']

        conv_coll = Conversation(request.app['db'])
        cursor = conv_coll.collection.find(query).limit(filter.limit).skip(filter.skip)
        data = await cursor.to_list(None)
        return data
