import asyncio

from aiokafka import AIOKafkaConsumer
from ujson import loads

from chat.db import Conversation, User


async def listen_to_amqp(app):
    conf = app['config']['amqp_service']
    consumer = AIOKafkaConsumer(
        conf['topic'],
        group_id=conf['consumer_group_id'],
        loop=asyncio.get_event_loop(),
        bootstrap_servers=conf['server'],
        auto_offset_reset='earliest',
        enable_auto_commit=True,
        heartbeat_interval_ms=3000,
        metadata_max_age_ms=300000,
        # value_deserializer=lambda x: loads(x.decode('utf-8')),
        key_deserializer=lambda x: x.decode('utf-8')
    )

    try:
        await consumer.start()
        # Consume messages
        async for msg in consumer:
            # print("consumed: ", msg.topic, msg.partition, msg.offset, msg.key, msg.value, msg.timestamp)
            try:
                value = loads(msg.value)
            except ValueError:
                # TODO: sentry
                continue
            if msg.key == 'upsert_user':
                user_coll = User(app['db'])
                await user_coll.update_one(**value)
            elif msg.key == 'upsert_dating':
                conv_coll = Conversation(app['db'])
                await conv_coll.update_one(**value)
    except asyncio.CancelledError:
        await consumer.stop()
    except Exception as e:
        # TODO: Sentry
        raise e
