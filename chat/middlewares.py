from aiohttp import web

from aiohttp_jwt import JWTMiddleware


@web.middleware
async def set_user_request(request, handler):
    """Проставим user_id в request"""
    user_id = str(request.get('payload', {}).get('user_id')) or None
    request['user_id'] = user_id
    return await handler(request)


def setup_middlewares(app):
    config = app['config']

    app.middlewares.extend([
        JWTMiddleware(config['secret_key'], whitelist=('/v1/subscriptions',)),
        set_user_request,
    ])
