### События

Сообщение, посылаемое в брокер, состоиз из **key** и **value**.

Пример:
```
key:value

upsert_user:{"id": "1", "username": "Привет"}
```

Key можно определить как "событие".

Используемые события:

- upsert_user
- upsert_dating

#### upsert_user

Данное событие отправляется при создании активного пользователя, которому доступен 

функционал чата. Также событие отправляется при изменнии свойств пользователя.

Трафарет value:
```
t.Dict({
    'id': t.Or(t.String, t.Int),
    t.Key('username', optional=True): t.String,
    t.Key('gender', optional=True): t.Enum('male', 'female'),
    t.Key('is_active', optional=True): t.Bool,
    t.Key('avatar_public', optional=True): t.URL,
    t.Key('avatar_private', optional=True): t.URL,
})
```

В БД обновляются только те ключи, которые присутствуют в value.

#### upsert_dating

Данное событие отправляется при начале знакомства, когда получено согласие начать знакомства и пользователям

становится доступность фунционал отправки личных сообщений.

Также событие отправляется при ограничии функционала отправки сообщений между участниками диалога.

Трафарет value:
```
t.Dict({
    'id': t.Or(t.String, t.Int),
    t.Key('participants', optional=True): t.List(t.Or(t.String, t.Int)),
    t.Key('is_active'): t.Bool,
})
```

В БД обновляются только те ключи, которые присутствуют в value.


### Командная строка: пример отправки событый

ssh root@51.15.60.70

/root/kafka_2.12-2.2.0/bin/kafka-console-producer.sh --broker-list localhost:9092 \
    --topic test2 \
    --property "parse.key=true" \
    --property "key.separator=:"

upsert_user:{"id": "1", "username": "Привет"}


Файл `Make` содержит примеры.

Пример команды
```
make publish_topic_user
```