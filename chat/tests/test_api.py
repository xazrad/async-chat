'''
Генерировать JWT https://codepen.io/jpetitcolas/pen/zxGxKN
payload
{
 "user_id": "1",
 "iat": 1561546302,
 "exp": 2593082290
}
'''

import json
from urllib.parse import urlencode

import pytest


TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMSIsImlhdCI6MTU2MTU0NjMwMiwiZXhwIjoyNTkzMDgyMjkwfQ.iG6LUoqxdLzU6T17ZalGBrNrPmIOplShLzrZi57CI7E'


j = lambda **kwargs: json.dumps(kwargs)


@pytest.mark.parametrize("token, status", [
    (TOKEN, 200),
    ("", 403)
])
async def test_index(cli, token, status):
    response = await cli.get('/v1/graphql', headers={'AUTHORIZATION': f'Bearer {token}'})
    assert response.status == status


@pytest.mark.parametrize("token, status", [
    (TOKEN, 200),
])
async def test_token(cli, token, status):
    data = '''
        mutation {
            wsToken {
              token
            }
        }

    '''

    response = await cli.post(
        '/v1/graphql',
        headers={
            'AUTHORIZATION': f'Bearer {token}',
            'CONTENT-TYPE': 'application/json'
        },
        data=j(query=data)
    )

    assert response.status == status
    data = await response.json()
    token = data['data']['wsToken']['token']

    ws = await cli.ws_connect(f'/v1/subscriptions/{token}', autoclose=True)

    req_init = {
        'id': '1',
        'type': 'connection_init',
        'payload': {}
    }

    await ws.send_str(json.dumps(req_init))
    r = await ws.receive()

    assert r.json()

    req_data = {
        'id': '1',
        'type': 'start',
        'payload': {
            'query': '''
            subscription {
                randomInt {
                  seconds
                  randomInt
                }
            }'''
        }
    }

    await ws.send_str(json.dumps(req_data))
    r = await ws.receive()

    random_data = r.json()['payload']['data']['randomInt']
    assert 'randomInt' in random_data
    assert 'seconds' in random_data

    await ws.close()
