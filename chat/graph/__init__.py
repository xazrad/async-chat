import graphene

from .mutations import MessageReaded, SendMessage, TypingMessage, WSToken

from .subscriptions import ChatSubscription
from .queries import ConversationQuery


class Query(ConversationQuery):
    pass


class Mutation(graphene.ObjectType):
    typing_message = TypingMessage.Field(description="...пишет...")
    ws_token = WSToken.Field(description="Получит token-url для подключения к ws")
    send_message = SendMessage.Field(description="Отправить сообщение")
    message_readed = MessageReaded.Field(description="Пометить сообщение прочитанным")


class Subscription(ChatSubscription):
    pass


schema = graphene.Schema(
    query=Query,
    mutation=Mutation,
    subscription=Subscription
)
