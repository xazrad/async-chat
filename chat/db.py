from datetime import datetime

import trafaret as t
from motor.motor_asyncio import AsyncIOMotorClient


USER_T = t.Dict({
    t.Key('id') >> '_id': t.Or(t.String, t.Int),
    t.Key('username', optional=True): t.String,
    t.Key('gender', optional=True): t.Enum('male', 'female'),
    t.Key('is_active', optional=True): t.Bool,
    t.Key('avatar_public', optional=True): t.URL,
    t.Key('avatar_private', optional=True): t.URL,
})


DATING_T = t.Dict({
    t.Key('id') >> '_id': t.Or(t.String, t.Int),
    t.Key('participants', optional=True): t.List(t.Or(t.String, t.Int)),
    t.Key('is_active'): t.Bool,
})


def _validate_data(data, Traf):
    try:
        _data = Traf.check(data)
    except t.DataError:
        # TODO: sentry send warning
        return
    return _data


class BaseModel:
    def __init__(self, db):
        self.db = db
        self.collection = db[self.__class__.__name__.lower()]
        self.cursor = None


class User(BaseModel):

    async def get(self, _id):
        result = await self.collection.find_one({'_id': _id})
        return result

    async def update_one(self, **kw):
        _validated = _validate_data(kw, USER_T)
        if not _validated:
            return
        _id = str(_validated.pop('_id'))
        _validated['date_update'] = datetime.utcnow()

        result = await self.collection.update_one(
            filter={'_id': _id},
            update={'$set': _validated},
            upsert=True
        )
        return result


class Message(BaseModel):
    """
    {
        _id: ObjectId
        conversation_id: Int,
        sender: String,
        recipient: String
        type: MessageTypeEmun,
        content: String,
        time_created: DateTime,
        is_readed: Bool
    }
    """

    async def create_message(self, conversation_id, sender, recipient, content):
        from chat.graph.types import MessageTypeEmun

        time_created = datetime.utcnow()

        output = {
            'conversation_id': conversation_id,
            'sender': sender,
            'recipient': recipient,
            'type': MessageTypeEmun.userMessage.value,
            'content': content,
            'time_created': time_created,
            'is_readed': False
        }

        result = await self.collection.insert_one(output)

        message_id = result.inserted_id

        output.update({'_id': message_id})

        return output

    async def make_readed(self, conversation_id, recipient, ids):

        the_filter = {
            'conversation_id': conversation_id,
            '_id': {'$in': ids},
            'recipient': recipient,
            'type': 'user_message'
        }
        r = await self.collection.update_many(
            filter=the_filter,
            update={'$set': {'is_readed': True}},
            upsert=False
        )
        return r.matched_count


class Conversation(BaseModel):
    '''
    {
        "participants": [String, String],
        "is_active": Bool,
    }
    '''
    async def update_one(self, **kw):
        _validated = _validate_data(kw, DATING_T)
        if not _validated:
            # TODO: послать событие в sentry
            return
        _id = str(_validated.pop('_id'))
        _validated['date_update'] = datetime.utcnow()
        # проставим str participants
        list(map(lambda x: str(x), _validated.get('participants', [])))

        result = await self.collection.update_one(
            filter={'_id': _id},
            update={'$set': _validated},
            upsert=True
        )
        return result

    async def get_by_id(self, _id, user_id):
        # TODO: кешировать
        result = await self.collection.find_one({"_id": _id, "participants": user_id}, {"_id": 1, "is_active": 1, "participants": 1})
        return result


async def init_db(app):
    conf = app['config']['mongo']
    client = AsyncIOMotorClient(conf['url'])
    app['db'] = client[conf['db_name']]
