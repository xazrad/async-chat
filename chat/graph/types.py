import graphene


# Enums
class ErrorsEnum(graphene.Enum):
    ConversationDoesNotExist = 'conversation_does_not_exist'
    ConversationIsNotActive = 'conversation_is_not_active'

    @property
    def description(self):
        if self == ErrorsEnum.ConversationDoesNotExist:
            return 'Диалога не существет'
        else:
            return 'Диалог отклонен'


class MessageDirectionEmun(graphene.Enum):
    input = 'output'
    output = 'output'


class MessageTypeEmun(graphene.Enum):
    systemMessage = 'system_message'
    userMessage = 'user_message'

    @property
    def description(self):
        if self == MessageTypeEmun.systemMessage:
            return 'Сообщение от системы'
        return 'Сообщение от пользователя'


# ObjectType
class RandomType(graphene.ObjectType):
    '''
    Random type. Need for test.
    '''
    seconds = graphene.Int()
    random_int = graphene.Int()


class UserObject(graphene.ObjectType):
    _id = graphene.String(required=True, description="ID объекта")
    username = graphene.String(description="Никнейм пользователя")


class MessageObject(graphene.ObjectType):
    _id = graphene.String(required=True, description="")
    direction = graphene.Field(MessageDirectionEmun, description="")
    type = graphene.Field(MessageTypeEmun, description="")
    content = graphene.String()
    time_created = graphene.DateTime()
    is_readed = graphene.Boolean()


class ConversationObject(graphene.ObjectType):
    _id = graphene.String(required=True, description="ID объекта")
    participant = graphene.Field(UserObject, description="Участник")
    is_active = graphene.Boolean(description="Статус диалога")
    messages = graphene.List(MessageObject, description="Сообщения")
    # TODO: количество новых сообщений


# Mutation Output
class ResultStatusOutput(graphene.ObjectType):
    ok = graphene.Boolean(required=True, description="true/false")
    error = ErrorsEnum(description="Описание ошибки")


class WsTokenOutput(ResultStatusOutput):
    token = graphene.String(description="url-token")


class MessageReadedOutput(ResultStatusOutput):
    matched_count = graphene.Int(description="Количество обновленных сообщений")


class SendMessageOutput(ResultStatusOutput):
    message = graphene.Field(MessageObject, description='Новосозданное сообщение')


# Inputs
class ConversationInput(graphene.InputObjectType):
    limit = graphene.Int(default_value=20, description="Кол. значений")
    skip = graphene.Int(default_value=0, description="")
    _id = graphene.String()
    participant = graphene.String()
