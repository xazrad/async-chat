### JWT (query & mutation)

Для HTTP запросов GraphQL (query & mutation) необходим  JWT токен.

Header
```
AUTHORIZATION: Bearer ${TOKEN}
```

#### Особенности формирования JWT

**Payload** JWT должен содержать ключ `user_id`.

Для тестового сервера JWT подписывается строкой **secret**.

[Cгенерировать JWT для тестового сервера](https://codepen.io/jpetitcolas/pen/zxGxKN)

Пример payload
```
{
 "user_id": "1",
 "iat": 1561546302,
 "exp": 2593082290
}
```

### Websocket & Subscriptions

Транспортом для GraphQl subscription является WebSocket. 

Для подключения к ws необходимо получить уникальную `ws-token-url` 

WS-Ссылка:
```
`ws://.../v1/subscriptions/{TOKEN}`
```

Для получения `ws-token-url` необходимо использовать mutation **WSToken**.

Безопасность:

- ws-url активен 5 секунд.
- Проверка заголовков `User-Agent`, `Accept`, `Accept-Language`. Заголовки при подключении к ws должны соотвествовать
  заголовкам при обращении к mutation **WSToken**