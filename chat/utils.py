import hashlib
import ujson
from decimal import Decimal
from datetime import date, datetime

from itsdangerous import URLSafeTimedSerializer
from itsdangerous.exc import BadSignature, SignatureExpired


class TokenWSGenerator:

    def __init__(self):
        self.s = URLSafeTimedSerializer('secret-key')
        self.max_age = 5

    def _get_fingerprint(self, request):
        keys = ['User-Agent', 'Accept', 'Accept-Language']

        finger = ''.join(
            map(lambda x: request.headers.get(x, ''), keys)
        )
        return hashlib.md5(finger.encode('utf-8')).hexdigest()

    def generate(self, request):
        fingerprint = self._get_fingerprint(request)
        user_id = request['user_id']
        sign = self.s.dumps([user_id, fingerprint])
        return sign

    def check(self, request, token):

        try:
            unsigned_data = self.s.loads(token, max_age=5)
        except (BadSignature, SignatureExpired):
            return
        fingerprint = self._get_fingerprint(request)

        user_id, fingerprint_in = unsigned_data

        if fingerprint != fingerprint_in:
            return

        return user_id


def _json_serial(obj):

    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    if isinstance(obj, date):
        return obj.strftime('%Y-%m-%d')
    if isinstance(obj, Decimal):
        return float(obj)


def dumps(data):
    return ujson.dumps(data, default=_json_serial)
