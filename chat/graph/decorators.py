from chat.db import Conversation
from chat.graph.types import ErrorsEnum, ResultStatusOutput


def check_access_conversation(f):
    async def wrap(cls, root, info, conversation_id, **kwargs):

        request = info.context['request']

        # проверить что такой диалог существует
        conv_coll = Conversation(request.app['db'])
        conv_doc = await conv_coll.get_by_id(conversation_id, request['user_id'])
        if conv_doc is None:
            return ResultStatusOutput(ok=False, error=ErrorsEnum.ConversationDoesNotExist)
        if not conv_doc['is_active']:
            return ResultStatusOutput(ok=False, error=ErrorsEnum.ConversationIsNotActive)

        kwargs['conv_doc'] = conv_doc

        return await f(cls, root, info, conversation_id, **kwargs)
    return wrap
