import aioredis
from unittest.mock import Mock

import pymongo
import pytest
from graphene.test import Client
from motor.motor_asyncio import AsyncIOMotorClient

from chat.app import init_app
from chat.graph import schema
from chat.settings import BASE_DIR, get_config


# TODO: генерить название
DB_NAME = 'test_2345678'

TEST_CONFIG_PATH = BASE_DIR / 'config' / 'chat_test.yaml'

# fixtures
@pytest.fixture(scope='session')
def mongo_client():
    client = pymongo.MongoClient('mongodb://localhost:27017')
    return client


@pytest.fixture
async def requests():
    '''
    In the graphene's client u should put request for get resource in program
    from app.
    '''
    from chat.utils import TokenWSGenerator

    # TODO: переделать и все брать ищ конфиг!!
    client = AsyncIOMotorClient('mongodb://localhost:27017')
    # TODO: придумать teardown
    pub = await aioredis.create_redis_pool('redis://localhost')

    class RequestMock:
        def __init__(self, *args, **kwargs):
            self._dict = {}

        def __setitem__(self, key, value):
            self._dict[key] = value

        def __getitem__(self, key):
            return self._dict[key]

    request = RequestMock()
    request['user_id'] = '1'
    request.headers = {}
    request.app = {
        'db': client[DB_NAME],
        'redis_pub': pub,
        'ws_token': TokenWSGenerator()
    }

    return {'request': request}


@pytest.yield_fixture(scope='session')
def collections(mongo_client):
    '''
    The fixture for create all collections and init simple data.
    '''
    db = mongo_client[DB_NAME]
    # TODO: переделать на лоад
    db.user.insert_many([
        {
            '_id': "1"
        },
        {
            '_id': "2"
        },
        {
            '_id': "3"
        },
        {
            '_id': "4"
        },
    ])
    db.conversation.insert_many([
        {
            '_id': '12',
            'participants': ["1", "2"],
        },
        {
            '_id': '31',
            'participants': ["3", "1"],
            'is_active': True
        },
        {
            '_id': '32',
            'participants': ["3", "2"]
        },

    ])

    db.message.insert_many([
        {
            '_id': '120',
            'conversation_id': '31',
            'sender': '1',
            'recipient': '3',
            'is_readed': False,
            'type': 'user_message'
        },
        {
            '_id': '121',
            'conversation_id': '31',
            'sender': '3',
            'recipient': '1',
            'is_readed': False,
            'type': 'user_message'
        },
        {
            '_id': '122',
            'conversation_id': '31',
            'sender': '1',
            'recipient': '3',
            'is_readed': False,
            'type': 'user_message'
        },
        {
            '_id': '123',
            'conversation_id': '31',
            'sender': '3',
            'recipient': '1',
            'is_readed': False,
            'type': 'user_message'
        },

    ])

    yield
    mongo_client.drop_database(DB_NAME)


@pytest.fixture(scope='session')
def client(collections):
    '''
    The fixture for the initialize graphene's client.
    '''
    return Client(schema, return_promise=True)


@pytest.fixture
def cli(loop, aiohttp_client):
    config = get_config(['', '-c', TEST_CONFIG_PATH.as_posix()])
    app = init_app(config)
    # return await aiohttp_client(app)
    return loop.run_until_complete(aiohttp_client(app))
