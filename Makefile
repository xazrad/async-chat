export PYTHONPATH=.

FLAGS=
PROJECT_NAME=chat


run:
	@python -m $(PROJECT_NAME)

flake:
	flake8 chat

test:
	py.test --pdb --cov-report term-missing --cov=chat

mongo_shell:
	docker exec -it nikah-chat-mongo mongo

publish_topic_user:
	ssh root@51.15.60.70 -t 'echo "upsert_user:{\"id\": \"4\", \"username\": \"Привет27\"}" | /root/kafka_2.12-2.2.0/bin/kafka-console-producer.sh \
							--broker-list localhost:9092 \
							--topic test2 \
							--property "parse.key=true" \
							--property "key.separator=:"'

publish_topic_dating:
	ssh root@51.15.60.70 -t 'echo "upsert_dating:{\"id\": 12, \"is_active\": true, \"participants\": [1,2]}" | /root/kafka_2.12-2.2.0/bin/kafka-console-producer.sh \
							--broker-list localhost:9092 \
							--topic test2 \
							--property "parse.key=true" \
							--property "key.separator=:"'

.PHONY: flake testflak