FROM python:3.7-alpine

ADD requirements.txt /requirements.txt

RUN set -ex \
    && apk add --no-cache --virtual .build-deps \
        gcc \
        make \
        libc-dev \
        musl-dev \
        libffi-dev \
        libgcc \
    && pip install --no-cache-dir -r /requirements.txt \
    && mkdir /config

WORKDIR /app/

COPY . /app/

COPY ./config/chat.yaml /config/chat.yaml

VOLUME [ "/config" ]

EXPOSE 8000

CMD ["python", "-m", "chat", "--config", "/config/chat.yaml"]
