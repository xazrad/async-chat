# Event-Driven Microservice Chat

Брокер: kafka

Интерфейс: GraphQL

Фреймворк: aiohttp


## Документация

### Брокер сообщений

- [События](docs/broker.md#events)
- [Командная строка: пример отправки событый](docs/broker.md#cli)

### Аудентификация

- [JWT (query & mutation)](docs/auth.md#events)
- [WebSocket (subsription)](docs/auth.md)

### GraphqQL

- [Схема](docs/graphql.md)

### Деплой

### Запуск

```
python -m chat --config=config/chat.yaml
```

Параметр --config опциональный
