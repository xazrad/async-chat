import argparse
import pathlib

import trafaret as T
from trafaret_config import commandline


BASE_DIR = pathlib.Path(__file__).parent.parent

DEFAULT_CONFIG_PATH = BASE_DIR / 'config' / 'chat.yaml'

TRAFARET = T.Dict({
    T.Key('amqp_service'):
        T.Dict({
            'consumer_group_id': T.String(),
            'server': T.String(),
            'topic': T.String(),
        }),
    T.Key('mongo'):
        T.Dict({
            'url': T.String,
            'db_name': T.String
        }),
    T.Key('host'): T.IP,
    T.Key('port'): T.Int,
    T.Key('secret_key'): T.String,
    T.Key('redis'):
        T.Dict({
            'url': T.String(),
        }),
})


def get_config(argv=None):
    ap = argparse.ArgumentParser()

    commandline.standard_argparse_options(ap, default_config=DEFAULT_CONFIG_PATH)

    # ignore unknown options
    options, unknown = ap.parse_known_args(argv)

    config = commandline.config_from_options(options, TRAFARET)
    return config
