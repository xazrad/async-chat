import asyncio
import logging
import sys
from functools import partial

import aioredis

from aiohttp import web

from .amqp import listen_to_amqp
from .db import init_db
from .middlewares import setup_middlewares
from .routes import setup_routes
from .settings import get_config
from .utils import TokenWSGenerator


async def start_background_tasks(app: web.Application) -> None:
    loop = asyncio.get_event_loop()
    app['amqp_task'] = loop.create_task(listen_to_amqp(app))


async def cleanup_background_tasks(app: web.Application) -> None:
    app['amqp_task'].cancel()
    await app['amqp_task']


async def init_redis(app: web.Application) -> None:
    redis_url = app['config']['redis']['url']

    pub = await aioredis.create_redis_pool(redis_url)

    create_redis = partial(aioredis.create_redis_pool, redis_url)

    app['redis_pub'] = pub
    app['create_redis'] = create_redis


async def close_redis(app: web.Application) -> None:
    app['redis_pub'].close()


def init_app(config: dict) -> web.Application:

    app = web.Application()

    app['config'] = config

    app['ws_token'] = TokenWSGenerator()

    # create db connection on startup, shutdown on exit
    app.on_startup.extend([init_db, init_redis, start_background_tasks])
    app.on_cleanup.extend([close_redis, cleanup_background_tasks])

    setup_routes(app)
    setup_middlewares(app)

    return app


def main(argv):
    logging.basicConfig(level=logging.DEBUG)
    config = get_config(argv)

    app = init_app(config)

    web.run_app(app, host=config['host'], port=config['port'])


if __name__ == '__main__':
    main(sys.argv[1:])
