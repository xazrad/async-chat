### Схема

Доступ к web-интерфейсу закрыт. Для просмотра схемы предлагаю использовать внешние инструменты.

[graphql-voyager](https://apis.guru/graphql-voyager/)

[Introspection query](https://gitlab.com/nikah-life/chat-backend/blob/master/develop/introspection_graphql.js)

Просмотр схемы:
```
CHANGE SCHEMA -> вкладка Introspection -> вставить данные из Introspection
```
